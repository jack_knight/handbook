将 Docker 容器从一台机器迁移到另一台机器（例如通过 U 盘）并在内网环境中运行，可以通过以下步骤实现。这个过程主要包括导出容器镜像、传输镜像到目标机器、导入镜像并运行容器。

### 步骤 1: 导出容器镜像

1. **找到容器的 ID 或名称**：
   ```sh
   docker ps -a
   ```

2. **将容器导出为镜像**（如果容器还没有被提交为镜像，需要先提交）：
   ```sh
   docker commit <container_id> my_image:latest
   ```

3. **导出镜像到文件**：
   ```sh
   docker save -o /path/to/usb/my_image.tar my_image:latest
   ```

### 步骤 2: 将镜像文件传输到 U 盘

1. **将导出的镜像文件复制到 U 盘**：
   ```sh
   cp /path/to/usb/my_image.tar /path/to/usb/
   ```

### 步骤 3: 在目标机器上导入镜像

1. **将 U 盘插入目标机器**。

2. **将镜像文件从 U 盘复制到目标机器**：
   ```sh
   cp /path/to/usb/my_image.tar /path/to/local/directory/
   ```

3. **导入镜像到 Docker**：
   ```sh
   docker load -i /path/to/local/directory/my_image.tar
   ```

### 步骤 4: 运行容器

1. **查看导入的镜像**：
   ```sh
   docker images
   ```

2. **运行容器**：
   ```sh
   docker run -d --name my_container my_image:latest
   ```

### 详细说明

#### 导出容器为镜像
- `docker commit <container_id> my_image:latest`：将运行中的容器提交为一个新的镜像。
- `docker save -o /path/to/usb/my_image.tar my_image:latest`：将镜像导出为一个 tar 文件。

#### 传输镜像文件
- `cp /path/to/usb/my_image.tar /path/to/usb/`：将导出的镜像文件复制到 U 盘。

#### 导入镜像到目标机器
- `cp /path/to/usb/my_image.tar /path/to/local/directory/`：将 U 盘中的镜像文件复制到目标机器上的某个目录。
- `docker load -i /path/to/local/directory/my_image.tar`：将 tar 文件导入为 Docker 镜像。

#### 运行容器
- `docker run -d --name my_container my_image:latest`：以 detached 模式运行容器，并命名为 `my_container`。