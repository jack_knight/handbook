```
#新建的linux容器可能缺少用于验证HTTPS连接的证书（ca-certificates），导致不能使用https的软件源，安装证书：
apt-get install -y ca-certificates

#默认情况下，Docker容器并不会自动安装或启用SSH服务器，安装并启用ssh：
apt-get install -y openssh-server
service ssh start
systemctl enable ssh

#root用户可能未设置密码，设密码：
passwd root
#允许root用户通过密码登录（这种方法相对不那么安全，因为如果有人获得了容器的IP地址和端口信息，他们就可以尝试暴力破解密码。），编辑/etc/ssh/sshd_config，找到并修改以下行：
PermitRootLogin yes
PasswordAuthentication yes
#重启SSH服务：
service ssh restart

#最安全的方式是配置SSH密钥认证，而不是依赖密码。以下是具体步骤：
#在你的本地机器上运行以下命令生成SSH密钥对：
ssh-keygen -t rsa -b 4096
#按提示操作，通常可以直接按回车接受默认设置。

#将公钥添加到容器中的authorized_keys文件，进入容器并创建或编辑/root/.ssh/authorized_keys文件，确保权限正确：
docker exec -it ubuntu-kvm bash
mkdir -p /root/.ssh
echo "你的公钥内容" >> /root/.ssh/authorized_keys
chmod 700 /root/.ssh
chmod 600 /root/.ssh/authorized_keys
#配置SSH服务允许密钥认证，编辑/etc/ssh/sshd_config，确保以下行未被注释且设置正确：
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
#重启SSH服务：
service ssh restart
```