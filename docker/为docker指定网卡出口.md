```
docker network create -d macvlan --subnet=221.3.220.186/24 --gateway=221.3.220.65 -o parent=ovs_eth2 mac220 创建docker网络
     -d 指定 Docker 网络 driver
    --subnet 指定 macvlan 网络所在的网络
    --gateway 指定网关
    -o parent 指定用来分配 macvlan 网络的物理网卡
    mac220 你取的网络名称

docker network ls 查看创建好的网络

docker run -d --name mht2-220.186 --hostname mht2 --ip=221.3.220.186 --network mac220 -v /volume1/docker/mht2-220.186:/mht_cache --restart=always --privileged=true yangnd89/mht:latest
     --ip 指定容器使用的 IP，这样做的目的是防止自动分配，造成 IP 冲突
    --network 指定 macvlan 网络

```