### 克隆 
``` 
通过SSH协议克隆 
git clone git@gitee.com:jack_knight/handbook.git
```

### 添加和提交
```
git add *.py
git commit -m "代码提交信息"
```

### 推送改动到远端仓库
> git push

### 从远端仓库更新到本地
> git pull

### 发布发行版
在[统计],[发行版]中点击[创建发行版],标注:v1.x,上传zip包

### 在错误的目录误使用git命令，想删除目录上的git信息
> rm -rf .git