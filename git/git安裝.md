### 安装
新立得搜git安装,或
> sudo apt-get install git

### 配置
配置个人的用户名称和电子邮件地址：
```
git config --global user.name "Jack Knight"
git config --global user.email jack_knight@qq.com
```
### 新电脑使用
在gitee设置,添加SSH 公钥
> ssh-keygen -t rsa -C "jack_knight@qq.com" 
 
按照提示完成三次回车，即可生成 ssh key。通过查看 ~/.ssh/id_rsa.pub 文件内容，获取到public key