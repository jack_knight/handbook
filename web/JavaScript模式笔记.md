### 1.运行前先使用JSlint检测代码
### 第二章 高质量JavaScript基本要点
* 减少全局对象

* 单var模式：
```
function func() {
	var a = 1,
		b = 2,
		sum = a + b,
		myobject = {},
		i,
		j,
		el = document.getElementById("result");
	// function body....
}
```

* for循环缓存住要遍历的数组的长度：
```
for (var i = 0, max = myarray.length; i < max; i++) {
	// do something with myarray[i]
}
```

* 将i++替换成为下面两者之一,提高代码的可读性：
```
i = i + 1
i += 1
```

* 推荐使用===和!==运算符

* setInterval()、setTimeout()和Function()构造函数传入字符串的情形和eval()类似，这种用法也是应当避免的.如果你不得不使用eval()，你可以尝试用new Function()来代替。这有一个潜在的好处，在new Function()中运行的代码会在一个局部函数作用域内执行，因此源码中所有用var定义的变量不会自动变成全局变量。

* 如果if或for中只有一个语句，花括号不要省略,将左花括号与上一条语句放在同一行。

* 句尾使用分号；符号之后使用空格；

* 首字母大写可以提示你这是一个构造函数，而首字母小写的函数一般只认为它是普通的函数，不应该通过new来调用它

* 对于构造函数，可以使用“大驼峰式”命名，比如MyConstructor()，对于函数和方法，可以采用“小驼峰式”命名，比如myFunction()，calculateArea()和getFirstName()。不是函数的变量所有字母都是小写，单词之间用下划线分隔，比如，first_name，favorite_bands和old_company_name，这种方法可以帮助你区分函数和其他标识符。JavaScript中无法定义常量（尽管有一些内置常量比如Number.MAX_VALUE），所以开发者都采用了这种命名习惯，对于那些程序运行周期内不会更改的变量使用全大写字母来命名。全局变量都大写。这种命名方式和“减少全局变量”的约定相辅相成。在名字尾部添加下划下以表明私有，比如name_和getElements_()，使用一个下划线前缀表明受保护的属性_protected，用两个下划线前缀表明私有属性__private