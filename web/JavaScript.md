### JSLint
JSLint是一个JavaScript代码质量检测工具。
```
//安装
npm install -g jslint 
//检测文件
jslint filename.js 
```

genay使用jslint，在“设置生成命令“中将jshint改为jslint，点“生成”菜单下的“Lint”就可。

### 页面跳转、页面重定向
```
// 类似 HTTP 重定向到菜鸟教程,该方法通过指定 URL 替换当前缓存在历史里（客户端）的项目.
// 因此当使用 replace 方法之后，你不能通过"前进"和"后退"来访问已经被替换的URL
window.location.replace("https://www.runoob.com");

// 类似点击菜鸟教程的链接（a 标签）
window.location.href = "https://www.runoob.com";
```