### rsync
deepin应该使用rsync来同步,空间只用200G,一晚上就同步完了,也不会有apt-mirror的问题.
ubuntu不应该使用rsync来同步,空间可能要10多T,一星期了还没同步完,应该用apt-mirror.
```
rsync -av --delete-after rsync.deepin.com::deepin/ /var/www/deepin/
rsync -avzP --delete-after rsync://rsync.mirrors.ustc.edu.cn/repo/ubuntu/ /volume1/web/ubuntu/
```
用apt-mirror会有各种问题.

### apt-mirror2
```
鉴于apt-mirror的各种问题，应使用docker运行apt-mirror2来使用。
挂载目录和mirror.list文件
## Paths to working directories只需要设置这个
set base_path         /var/spool/apt-mirror
## All mirrors will be saved to mirror_path默认是有这3个，但不要设，否则客户端找不到文件，安装不了，应注释掉
#set mirror_path       $base_path/mirror
## Initial metadata synchronization path
#set skel_path         $base_path/skel
## Path to variable data
#set var_path          $base_path/var

## Used when arch is not explicitly specified for repository
set defaultarch       amd64
set nthreads          8
## Rate limit in bytes per second. Suffixes `k` and `m` are allowed
# set limit_rate        1k
set _tilde            0

## Mirrors are configured using standard sources.list syntax
## Binary mirrors
#deb  http://archive.ubuntu.com/ubuntu mantic main restricted universe multiverse这里设cpu好像没用
deb  http://mirrors.ustc.edu.cn/ubuntu/ noble main restricted universe multiverse
deb  http://mirrors.ustc.edu.cn/ubuntu/ noble-security main restricted universe multiverse
deb  http://mirrors.ustc.edu.cn/ubuntu/ noble-updates main restricted universe multiverse

```

### 安装apt-mirror
> sudo apt-get install apt-mirror
### 修改apt-mirror配置文件

在修改配置文件之前，我们首先要确定自己系统的版本，命令： 
```
$sudo lsb_release -a
	No LSB modules are available.
	Distributor ID:	Ubuntu
	Description:	Ubuntu 16.04 LTS
	Release:	16.04
	Codename:	xenial
```
Codename代号的意思，16.04代号xenial，所以我们接下来的配置文件跟xenial有关，当然14.04代号是trusty，一样的操作。
```
sudo nano /etc/apt/mirror.list
############# config ##################
# 以下注释的内容都是默认配置，如果需要自定义，取消注释修改即可
set base_path /var/spool/apt-mirror
#
# 镜像文件下载地址
# set mirror_path $base_path/mirror
# 临时索引下载文件目录，也就是存放软件仓库的dists目录下的文件（默认即可）
# set skel_path $base_path/skel
# 配置日志（默认即可）
# set var_path $base_path/var
# clean脚本位置
# set cleanscript $var_path/clean.sh
# 架构配置，i386/amd64，默认的话会下载跟本机相同的架构的源
set defaultarch amd64
# set postmirror_script $var_path/postmirror.sh
# set run_postmirror 0
# 下载线程数
set nthreads 20
set _tilde 0
#
############# end config ##############

deb http://archive.ubuntu.com/ubuntu focal main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu focal-security main restricted universe multiverse
deb http://archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse
#deb http://archive.ubuntu.com/ubuntu focal-proposed main restricted universe multiverse
#deb http://archive.ubuntu.com/ubuntu focal-backports main restricted universe multiverse

#deb-src http://archive.ubuntu.com/ubuntu focal main restricted universe multiverse
#deb-src http://archive.ubuntu.com/ubuntu focal-security main restricted universe multiverse
#deb-src http://archive.ubuntu.com/ubuntu focal-updates main restricted universe multiverse
#deb-src http://archive.ubuntu.com/ubuntu focal-proposed main restricted universe multiverse
#deb-src http://archive.ubuntu.com/ubuntu focal-backports main restricted universe multiverse

clean http://archive.ubuntu.com/ubuntu

```
软件默认是artful版本,要改为对应的版本代号.注释掉源代码deb-src可减少下载量.

### 【问题描述】不同步文件名中有“@”的文件
```
vim /usr/bin/apt-mirror

注释掉以下行

sub sanitise_uri
{
    my $uri = shift;
    $uri =~ s[^(\w+)://][];
    #$uri =~ s/^([^@]+)?@?// if $uri =~ /@/;
    $uri =~ s&:\d+/&/&;                       # and port information
    $uri =~ s/~/\%7E/g if get_variable("_tilde");
    return $uri;
}
```
 

### 【问题描述】不同步"cnf"目录下文件
 
```
vim /usr/bin/apt-mirror

450行增加 “ add_url_to_download( $url . $_ . "/cnf/Commands-" . $arch . ".xz" );”
 

            add_url_to_download( $url . $_ . "/binary-" . $arch . "/Release" );
            add_url_to_download( $url . $_ . "/binary-" . $arch . "/Packages.gz" );
            add_url_to_download( $url . $_ . "/binary-" . $arch . "/Packages.bz2" );
            add_url_to_download( $url . $_ . "/binary-" . $arch . "/Packages.xz" );
            add_url_to_download( $url . $_ . "/cnf/Commands-" . $arch . ".xz" );
            add_url_to_download( $url . $_ . "/i18n/Index" );
```

### 开始同步
> sudo apt-mirror