# 网络
未安装虚拟机前应规划好网络，因为网络生效要重启pve
* 在web中增加局域网vmbr
* 在web中增加NAT网
```
auto vmbr2
iface vmbr2 inet static
address 192.168.3.254
netmask 255.255.255.0
bridge_ports none
bridge_stp off
bridge_fd 0
post-up echo 1 > /proc/sys/net/ipv4/ip_forward
post-up iptables -t nat -A POSTROUTING -s ’192.168.3.0/24’ -o vmbr0 -j MASQUERADE
post-down iptables -t nat -D POSTROUTING -s ’192.168.3.0/24’ -o vmbr0 -j MASQUERADE
post-up iptables -t nat -A PREROUTING -i vmbr0 -p tcp --dport 3333 -j DNAT --to 192.168.3.100:3389
post-down iptables -t nat -D PREROUTING -i vmbr0 -p tcp --dport 3333 -j DNAT --to 192.168.3.100:3389
```
重启网络生效 service networking restart,如不生效，则只能重启系统。

# 硬盘
* 增加硬盘，首先将整块硬盘创建一个分区。然后使用静默方式创建一个物理卷(PV),并设置元数据大小为 250K，然后在/dev/sdb1 上创建卷组“vgname”。在卷组上创建“薄模式”存储池
```
sgdisk -N 1 /dev/sdb
pvcreate --metadatasize 250k -y -ff /dev/sdb1
vgcreate vgname /dev/sdb1
lvcreate -L 29.1t -T -n poolname vgname
```

# img磁盘转换
* 上传img
```
sftp -o Port=2222 root@192.168.119.3
put /path/filename(本地主机) /path/filename(远端主机)
```
* 转换img
在shell中执行
```
qm importdisk 101 synoboot.img local-lvm
```
101是虚拟机编号，synoboot.img是刚才上传的引导镜像，local-lvm是资源池

