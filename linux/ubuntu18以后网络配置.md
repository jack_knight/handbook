1. 首先查看网卡名

> ip a / ifconfig

2. cp一份yaml文件，改个名字
	
> $ sudo cp /etc/netplan/*.yaml /etc/netplan/网卡名.yaml

3. 修改yaml文件内容，如下：
```
# Let NetworkManager manage all devices on this system
network:
 version: 2
 renderer: NetworkManager
 ethernets:
   网卡名:
    dhcp4: no
    addresses: [ip/prefix]
    gateway4: gateway
    nameservers:
    addresses: [dns]
```
4. 启动
> sudo netplan apply