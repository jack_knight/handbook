# 系统
```
top 系统监视
passwd 当前用户修改密码
vi /etc/hostname 修改计算机名
lsblk 查看硬盘情况

#可以不分区，直接格式化
sudo mkfs -t ext4 /dev/sdb

#分区
fdisk /dev/sda 
#格式化
sudo mkfs -t ext4 /dev/sda1 

#挂载
sudo mount /dev/sda1 /kvm
#查看挂载
df -h
#开机自动挂载
sudo nano /etc/fstab
/dev/sda1 /kvm ext4 defaults 0 0

shutdown -h now关机
shutdown -r now重启

locale查看语言，/etc/locale.conf配置语言为gb2312

ctrl+c 中断程序
ctrl+d 退出shell
ctrl+a 光标移到行首
ctrl+e 光标移到行尾

sudo dpkg -i *.deb 安装deb软件

```
# 文件
```
sudo apt install lrzsz 安装lrzsz后可用rz上传，sz下载,deepin可以右键在终端中上传下载

ls 查看目录下文件 -a 可看隐藏文件

ll 查看文件详细清单

more 分页查看文本文件

touch 创建空白文件

mkdir 创建文件夹

cp 源文件 复制文件

mv 移动文件，改名

rm 删除文件

rm -rf  删除文件或目录，不管该目录下是否有子目录或文件，都直接删除

压缩：tar -cjvf [filename] [要压缩的文件或目录] 

解压：tar -xvf [filename] 
```
# 网络
```
dmesg | grep -in eth 列出网卡，状态

/etc/sysconfig/network-scripts/ifcfg-eth0 配置IP的文件

/etc/sysconfig/network 配置主机名的文件

/etc/resolv.conf 配置DNS的文件

ifconfig eth0 down

ifconfig eth0 192.168.0.1 netmask 255.255.255.0

ifconfig eth0 up

/etc/init.d/network restart?将手动的设置全部取消，使用原有的设定值重建网络参数

ifup eth0 以配置文件来启动

systemctl restart network 重新加载网卡信息
```

ubuntu系安装新立德 apt-get install synaptic

centos安装软件 yum install minicom


