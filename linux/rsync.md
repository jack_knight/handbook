
### 同步到群辉

> rsync -avz /home/jack jack@172.17.2.253:/volume1/jack-bak

> rsync -avz -e "ssh -p 2222" --include={"图片","文档","音乐","bin","jack"} --exclude="/*" /home/jack/ jack@172.17.1.103:/volume1/homes/jack/bak

### 服务器配置
#### ubuntu 16.04默认已安装rsync，rsync服务默认不是启动的，我们要修改rsync文件。

> sudo nano /etc/default/rsync

修改配置项：

> RSYNC_ENABLE=true   #false改true

#### 创建配置文件

将rsyncd.conf copy到/etc目录下

> sudo cp /usr/share/doc/rsync/examples/rsyncd.conf /etc

打开此配置文件，自行配置。

> sudo nano /etc/rsyncd.conf

其中path是需要同步的路径，你可以暂时只对此项配置，然后即可测试。

#### 启动
> sudo systemctl start rsync

#### 开机启动
> sudo systemctl enable rsync

### 客户端

客户端不需要进行特殊的配置，直接同步即可

> rsync -vzrtopg --progress  172.16.29.10::ftp ./  

如果你的配置文件中配置了auth users，客户端同步文件命令需指定账号，如下：

> rsync -vzrtopg –progress 账号@172.16.29.10::ftp ./


### 附配置文件说明
```
# sample rsyncd.conf configuration file

# GLOBAL OPTIONS

#motd file=/etc/motd #登录欢迎信息
#log file=/var/log/rsyncd #日志文件
# for pid file, do not use /var/run/rsync.pid if
# you are going to run rsync out of the init.d script.
pid file=/var/run/rsyncd.pid

#指定rsync发送日志消息给syslog时的消息级别，常见的消息级别是：uth, authpriv, cron, daemon, ftp, kern, lpr, mail, news, security, sys-log, user, uucp, local0, local1, local2, local3,local4, local5, local6和local7。默认值是daemon。
#syslog facility=daemon

#自定义tcp选项，默认是关闭的
#socket options=

#以下是模块信息，我们可以创建多个模块
# MODULE OPTIONS

[ftp]

        comment = public archive #模块描述
        path = /var/www/pub #需要同步的路径
        use chroot = yes #默认是yes|true，如果为true，那么在rsync在传输文件以前首先chroot到path参数指定的目录下。这样做的原因是实现额外的安全防护，但是缺点是需要root权限，并且不能备份指向外部的符号连接指向的目录文件。
#       max connections=10 #最大连接数
        lock file = /var/lock/rsyncd #指定支持max connections参数的锁文件。
# the default for read only is yes...
        read only = yes #只读选项
        list = yes #客户请求时可用模块时是否列出该模块
        uid = nobody #设定该模块传输文件时守护进程应该具有的uid
        gid = nogroup #设定该模块传输文件时守护进程应具有的gid，此项与uid配合可以确定文件的访问权限
#       exclude = #用来指定多个由空格隔开的多个模式列表，并将其添加到exclude列表中。这等同于在客户端命令中使用--exclude来指定模式，不过配置文件中指定的exlude模式不会传递给客户端，而仅仅应用于服务器。一个模块只能指定一个exlude选项，但是可以在模式前面使用"-"和"+"来指定是exclude还是include    #这个我的理解是排除目录中不需同步的文件
#       exclude from = #可以指定一个包含exclude模式定义的文件名
#       include = #与exclude相似
#       include from = #可以指定一个包含include模式定义的文件名
#       auth users = #该选项指定由空格或逗号分隔的用户名列表，只有这些用户才允许连接该模块。这里的用户和系统用户没有任何关系。如果"auth users"被设置，那么客户端发出对该模块的连接请求以后会被rsync请求challenged进行验证身份这里使用的 challenge/response认证协议。用户的名和密码以明文方式存放在"secrets file"选项指定的文件中。默认情况下无需密码就可以连接模块(也就是匿名方式)
#       secrets file = /etc/rsyncd.secrets #该文件每行包含一个username:password对，以明文方式存储，只有在auth users被定义时，此选项才生效。同时我们需要将此文件权限设置为0600
        strict modes = yes #该选项指定是否监测密码文件的权限，如果该选项值为true那么密码文件只能被rsync服务器运行身份的用户访问，其他任何用户不可以访问该文件。默认值为true
#       hosts allow = #允许的主机
#       hosts deny = #拒绝访问的主机
        ignore errors = no #设定rsync服务器在运行delete操作时是否忽略I/O错误
        ignore nonreadable = yes #设定rysnc服务器忽略那些没有访问文件权限的用户
        transfer logging = no #使rsync服务器使用ftp格式的文件来记录下载和上载操作在自己单独的日志中
#       log format = %t: host %h (%a) %o %f (%l bytes). Total %b bytes. #设定日志格式
        timeout = 600 #超时设置(秒)
        refuse options = checksum dry-run #定义一些不允许客户对该模块使用的命令选项列表
        dont compress = *.gz *.tgz *.zip *.z *.rpm *.deb *.iso *.bz2 *.tbz #告诉rysnc那些文件在传输前不用压缩，默认已设定压缩包不再进行压缩
```


rsync 命令格式:
```
    rsync [OPTION]... SRC [SRC]... [USER@]HOST:DEST
    rsync [OPTION]... [USER@]HOST:SRC DEST
    sync [OPTION]... SRC [SRC]... DEST
    rsync [OPTION]... [USER@]HOST::SRC [DEST]
    rsync [OPTION]... SRC [SRC]... [USER@]HOST::DEST
    rsync [OPTION]... rsync://[USER@]HOST[:PORT]/SRC [DEST]
```
常用的方式：

1.   备份本地文件 ==cp
> rsync -avzP   /home  /backup        ==相当于cp     
2.   同步本地到远程主机
> rsync -avzP /home/backup/  root@192.168.1.4:192.168.1.4:/share/rsysnctest/

3.   远程同步到本地

> rsync -avz root@192.168.1.4:192.168.1.4:/share/rsysnctest/ /home/aloft/

4.  找出文件直接的不同

> rsync -avz root@192.168.1.4:192.168.1.4:/share/rsysnctest/ /home/aloft/

5.  与contab 结合，定时备份

创建定时备份任务，A主机上将要备份/home/data 文件同步到B主机(172.25.0.150) 下/backup目录。

每天的晚上23:30 执行备份任务 
```
vim AtoBbacku.sh
#!/bin/bash
rsync -avzP  --delete  --password-file=rsyncd.secrets /home/data  root@172.25.0.150::/backup
chmod 755 AtoBbacku.sh
crontab -e
30 23 * * *   sh -x /AtoBbacku.sh
```

rsync 常用参数：

-a, --archive 归档模式，等于-rlptgoD;   #最常用的  一般直接 -avzP

-r, --recursive 递归模式;

-l 保持符号连接;

-p 保持文件原有权限;

-t 保持文件原有修改时间;

-g 保持文件原有用户组;

-o 保持文件原有属主;

-D -devices -specials，保持块设备文件和特殊文件;

-I 强制扫描,放弃“quick check”策略(quick check策略,通过查看文件的时间戳和文件大小,先排除一批认为相同的文件[rsync不会对文件夹本身做“quick check”]);

-z, --compress 压缩,默认的压缩算法和gzip一样;

-v, --verbose 显示进度详细信息;

-q, --quiet 精简输出模式;

-R, --relative 保持全路径;

--progress  显示传输进度(百分百);  等同于 -P

--delete 本地删除某一文件,远程也同步的删除该文件,保持严格的一致(不加该参数,则表示只增不减);

--exclude="*.tmp" 排除某些文件;

--exclude-from=FILE 排除FILE中指定模式的文件;

--port=PORT 指定其他的rsync服务端口;