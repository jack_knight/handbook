### 服务端
```
sudo apt install nfs-kernel-server 安装

sudo chown nobody:nogroup /mnt/ShareFolder
sudo chmod -R 777 /mnt/ShareFolder/ 使所有客户端都可以访问该共享文件夹里面的内容

sudo nano /etc/exports 
/mnt/ShareFolder 192.168.43.137(rw,sync,no_subtree_check) 允许一个ip访问
/mnt/ShareFolder 192.168.43.*(rw,sync,no_subtree_check) 允许一个网段的客户端访问

sudo exportfs -arv 启用并生效
```
### 客户端
```
sudo apt install nfs-common 安装

sudo showmount -e 192.168.3.167 显示NFS服务器目录

sudo mkdir -p /home/jack/iso 创建本地挂载目录

mount -t nfs 192.168.3.167:/iso /home/jack/iso linux挂载命令

mount 192.168.3.167:/iso x：win7挂载命令，用z盘可能有问题

sudo nano /etc/fstab 开机自动挂载
加入172.25.254.141:/ftp /ftp nfs defaults 0 0
```