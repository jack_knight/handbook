sudo pacman -Syyu更新当前系统到最新版本

# 网络
systemctl enable systemd-networkd.service 	开机启动网络服务

systemctl enable systemd-resolved.service		开机启动DNS服务
# 桌面
sudo pacman -S xorg 		安装X

sudo pacman -S xfce4 xfce4-goodies	安装桌面环境和相关软件
## 配置xfce4
cp /ect/X11/xinit/xinitrc ~/.xinitrc

nano ~/.xinitrc

将最后一行用#注释掉

        #exec xterm -geometry 80x66+0+0 -name login

添加exec startxfce4
