应安装win8以上系统，因virtio驱动只有win8以上才能安装，win7装不了。
### 管理
```
安装virt-manager管理。
#如遇“unable to connect to libvirt”错误，将当前用户加入libvirt用户组，注销：
sudo usermod -aG libvirt ethan

#远程管理kvm，建议首选tcp，不用ssh登录，使用自定义URL添加连接：
qemu+tcp://192.168.0.1:16509/system

#使用ssh连接，产生virt-manager主机公钥（用于virt-manager与libvirt免密钥登录）
ssh-keygen
#将virt-manager主机公钥复制给libvirt所在主机
ssh-copy-id -i ~/.ssh/id_rsa.pub -p 2201 root@宿主机IP地址

#使用ssh密码连接，随便安装一个ssh-askpass输入密码，建议安装ksshaskpass，选第一项，再勾“记住密码”可保存密码，不用多次输，不能用ssh-askpass-gnome、ssh-askpass，不能保存密码，要输十多次。

安装virt-viewer远程，virt-manage中的spice要选所有接口才连得上。
```
### 共享文件夹
```
virt-manager里面添加通道。"Add Hardware", then select "Channel". Add a "Spice port" device type with the "org.spice-space.webdav.0" name. 
Windows虚拟机，安装spice-webdavd软件。 
windows虚拟机启动spice-webdavd服务。 
remote-viewer连接 
```
### 共享文件 
虚拟机的配置,硬盘下有一个共享选项，勾上，重启，就可以直接拖了

### 默认有共享剪贴板
关机，添加硬件，添加‘信道’（channel spice），选“com.redhat.spice.0” 
然后在KVM虚拟机里装设 spice-vdagent 套件，即可。

### 内网 
自带nat功能，只需安装rinetd做端口转发，就可实现3389，22等端口的穿透。