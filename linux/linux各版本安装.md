### 发行版选择
默认安装ubuntu mate+星火应用商店，兼顾配置低和高颜值。  
配置高,闪存盘的电脑直接装deepin，不折腾。同一台电脑，ubuntukylin比deepin要卡要慢。  
配置低,机械盘的直接装ubuntu+星火应用商店，建议lubuntu32位，xubuntu无新立得，软件仓库慢得要死，64位打开WPS、浏览器慢得要死。  
WPS只能安装在64位系统里。   

### 分区
100g硬盘分4个主分区：  
* efi分区200m
* /boot分区，100m,此分区应该分大一点，否则ubuntu安装不上，或不单独创建此分区。
* /分区，30g
* windows分区30g左右
* 数据分区，剩下的
* 不建议单独创建swap分区，如需要，参见本书《创建和删除swap文件》，lubuntu会在/自动创建swap文件。

### 更换软件源
```
#2404以后配置文件在这
sudo cp /etc/apt/sources.list.d/ubuntu.sources /etc/apt/sources.list.d/ubuntu.sources.bak
#指定cpu架构
Architectures: amd64
#备份
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak

#自动修改
sudo sed -i 's@//.*archive.ubuntu.com@//mirrors.ustc.edu.cn@g' /etc/apt/sources.list
sudo sed -i 's/security.ubuntu.com/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
sudo sed -i 's/http:/https:/g' /etc/apt/sources.list

#手动修改
sudo nano /etc/apt/sources.list
ctrl+\ 输入原地址
回车后再输入现在地址 mirrors.163.com

#刷新
sudo apt-get update

#报错处理  
#如果是个人自建的apt源，如果只有64位，应将deb替换为deb [arch=amd64]，避免出错。  
#如果是麒麟等ubuntu，提示focal-backports没有Release文件，应将focal-backports行前加#注释掉。  
#如果不能使用https源，安装apt-get install -y ca-certificates
```

### 软件
```
apt-cache search 搜索软件
apt-get install 安装软件

synaptic，新立得
network-manager 命令行图形管理网卡，安装后使用nmtui命令打开
gnome-disk-utility 磁盘工具，可以分区、格式化、刻录img、iso镜像到u盘，测试硬盘
isomaster ISO镜像编辑，新版已无此软件，请用mkisofs命令：mkisofs -J -o xx.iso ./iso-folder/
brasero 光盘刻录
ibus，适合gtk，有双拼，可以输入日期，有云词库。
fcitx，适合qt，有双拼，不能输入日期，有云词库，可输入新词，ubuntu22.04firefox显示不了候选框。

nautilus-open-terminal，右键中添加"在终端中打开"
pinta,这个软件会把图片自动缩放到适合屏幕大小，用于小孩做题
lossless-cut，视频剪切，无损快速
```
### 问题处理
#### 电脑屏幕特殊，安装不了deepin
从deepin安装镜像启动，当任意一个“Install Deepin”高亮时，按 e（uefi启动）或者Tab键（ Legacy启动），在显示的代码中用方向键移动光标，删去“livecd-installer ”（也有人认为是删去“-installer ”）（包括一个空格） ，按F10（uefi启动）或者回车（legacy启动），等待一会，可进入deepin的Live模式。在live模式就可以显示完整，可以顺利安装了。

#### win_linux双系统时间不一致
`sudo timedatectl set-local-rtc 1`让ubuntu将系统时间和BIOS时间同步
#### 时间同步
```
#安装 chrony
sudo apt-get install chrony
#编辑
sudo nano /etc/chrony/chrony.conf
#修改为阿里云的 NTP 
pool ntp.aliyun.com iburst
#开机启动
sudo systemctl enable chrony
#立即启动
sudo service chrony start
```
#### ubuntu开机反复报错
```
“检测到系统程序出现问题 您想要立刻报告这个问题吗？“
sudo nano /etc/default/apport
把文件最后一行的enabled=1改为enabled=0。保存并关闭文件。完成之后你就再也不会看到弹窗报告错误了。
```
#### home目录变动
用sudo移动home目录后，home目录的所有者和所属组会变为root，会导致wine程序启动不了，还有程序访问不了home目录，需要到fstab文件中强制指定所有者。

