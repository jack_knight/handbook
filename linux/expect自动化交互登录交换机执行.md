spawn               交互程序开始后面跟命令或者指定程序

expect              获取匹配信息匹配成功则执行expect后面的程序动作

send exp_send       用于发送指定的字符串信息

exp_continue        在expect中多次匹配就需要用到

send_user           用来打印输出 相当于shell中的echo

exit                退出expect脚本

eof                 expect执行结束 退出

set                 定义变量

puts                输出变量

set timeout         设置超时时间interact 　　　　　　 允许用户交互

```
#cat myjob1.sh#!/usr/bin/expect -fspawn telnet xxx.xxx.xxx.xxx       #远程机器
IPexpect -re "login"send "yourusername\n"          #登录远程机器的用户名
expect -re "Password"send "yourpw\n"          #登录远程机器的密码
expect -re "#"send "date\n"          #登录后要在远程机器上执行的本地命令，这是只是查询远程机器的当地时间，你可以换成其他本地命令或脚本
expect -re "#"send "exit\n"          #完成后退出
interact
```
