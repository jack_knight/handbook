## 安装cups
## 配置cups
```
cp /etc/cups/cupsd.conf /etc/cups/cupsd.conf.bak
sudo nano  /etc/cups/cupsd.conf

Listen 0.0.0.0:631
<Location />
  Order allow,deny
  Allow all
</Location>
<Location /admin>
 Order deny,allow
Allow all
</Location>
```
## 重启cups服务
        /etc/init.d/cups restart

## 添加打印机
网页登录http://ip:631  
在cups管理界面中的“Administration”下的“Printers”下点击“Add Printer”，勾选“sharing”，品牌驱动选择"Raw"。  
## windows链接打印机
- 先安装打印机驱动
- 添加网络打印机，在“按名称选择共享打印机”中填入cups上打印机的完整地址：http://192.168.1.199:631/printers/hp-7110-series

