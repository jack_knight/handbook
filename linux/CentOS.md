```
#修改ip，hostmame
#使用 nmtui 工具修改IP地址
nmtui
#修改完后退出 nmtui 工具，并重启网络服务以应用修改：
sudo systemctl restart network

#修改密码
passwd

#CentOS用光驱中光盘搭建本地yum源
#挂载光盘：
mkdir /media/CentOS
mount /dev/dvd /media/CentOS

# 修改默认源文件名，使无能
mv CentOS-Base.repo CentOS-Base.repo.backup

# 系统默认有/etc/yum.repos.d/CentOS-Media.repo这个文件：
[root@centos yum.repos.d]# cat CentOS-Media.repo  
[c6-media]
name=CentOS-$releasever - Media
baseurl=file:///media/CentOS/
        file:///media/cdrom/
        file:///media/cdrecorder/
gpgcheck=1
enabled=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-6

#修改之，使能
vi CentOS-Media.repo  
enabled=1
#完成
```
