### 修改默认启动项，倒计时
```
sudo nano /etc/default/grub

#默认启动项，按列表的顺序，从0开始，如果要用上次的启动项，填saved
GRUB_DEFAULT=saved 
#如果要用saved，还需要下面这行
GRUB_SAVEDEFAULT=true 
#显示菜单，如果是hide就隐藏菜单,注释掉就显示菜单
GRUB_TIMEOUT_STYLE=menu 
#启动选择超时秒数,设置为-1取消倒计时
GRUB_TIMEOUT=3

//更新执行
sudo update-grub
```
### 添加win启动项
```
sudo nano /etc/grub.d/40_custom

menuentry 'win10' {
insmod part_gpt
insmod chain
insmod ntfs
insmod search_fs_uuid
search --fs-uuid --no-floppy --set=root 4BC2-0277 
chainloader ($root)/EFI/Microsoft/Boot/bootmgfw.efi
}
```