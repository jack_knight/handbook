开机将第二块硬盘挂载在“下载”目录
```
# 编辑fstab文件
sudo nano /etc/fstab

# 第一个是你的硬盘,blkid命令可以查看UUID，后面是要挂载的目录，参数uid是用户id，gid是组id，通过'id 用户名'得到，如果不写的话，浏览器下载时会报错
/dev/sda1 /home/jack/Downloads	ntfs    defaults,uid=1000,gid=1000        0       2

# 写完后检查配置文件是否有错误，否则开机启动失败
sudo mount -a
```