# 查看：
```
pes、pedisplay                查看pe的大小(pes==pescan)
pvs、pvdisplay                查看物理卷
vgs、vgdisplay、              查看卷组
lvs、lvdisplay、              查看逻辑卷
fdisk -l                   查看磁盘分区
```
# 创建：
```
pvcreate 设备路径               创建物理卷
如提示：Device /dev/sdb not found (or ignored by filtering)
则执行：dd if=/dev/urandom of=/dev/sdb bs=512 count=64

vgcreate 名字 pv路径            创建卷组

lvcreate -n 名字 -L 大小 vg名   创建逻辑卷
全部空间划为一个盘：lvcreate -l 100%VG -n lv_name vg_nam
创建精简存储池：lvcreate -L 80G -T -n vmstore vmdata 

格式化：mkfs.ext4 lv完整路径               格式化逻辑卷（mkfs.文件系统格式或-t 文件系统格式）

挂载：mount  lv完整路径  挂载点          挂载使用（可以使用/etc/fstab或autofs）
```
# 逻辑卷删除：
1. 卸载：umount 
2. 删lv：lvremove lv完整路径 
3. 删vg：vgremove vg名 
4. 删PV：pvremove 设备完整路径 去硬盘

# 逻辑卷扩展：
1. 扩展pv：相当于创建pv 
2. 扩展vg： vgextend vg名 新增pv路径 
3. 扩展lv： lvextend -L +扩展量 lv完整名   
扩展lv使用vg剩余的所有空间：lvextend -l +100%FREE /dev/vg1/lv1  
4. 刷新文件系统：resize2fs lv完整路径   
注意：灵活运用，看实际情况，注意顺序 （支持在线操作）  

# 逻辑卷的缩小：
1. 首先进行卸载 umount 检查文件系统：e2fsck -f lv完整路径
2. 减少文件系统：resize2fs lv完整路径 减少到的大小
3. 减少lv卷大小：lvreduce -L -减少量的大小 lv的完整路径  
减小需谨慎，文件系统的减小后大小一定要和lv卷最终大小相等  
