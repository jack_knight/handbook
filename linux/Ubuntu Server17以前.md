### 安装

windows只需要选择win32diskimager制作U盘安装程序，就可以正常安装Ubuntu Server。其他软件都不行。  
深度os可用"磁盘""从磁盘映像恢复",其他linux 用dd命令:  
`sudo dd if=/home/jack/Downloads/ubuntu-14.04.6-server-i386.iso of=/dev/sdb`
其他都不行。  
Linux交换分区可为物理内存的两倍  

### 网卡设置
```
/etc/network/interfaces
auto eth0
iface eth0 inet static
    address 192.168.1.10
    netmask 255.255.255.0
    gateway 192.168.1.1
auto eth1
iface eth1 inet dhcp
```
### 域名服务器设置
```
/etc/resolv.conf
search localdomain
nameserver 192.168.1.1
nameserver 192.168.1.2
```
关网卡`ifconfig eth0 down`   
启网卡`ifconfig eth0 up`  
重启网络 `sudo /etc/init.d/networking restart`  
执行`ethtool -p eth0`时，eth0对应的网口的灯就会闪烁，你可以分别调用ethtool判断eth1,eth2对应的网口；注意要在不插网线的时候进行，否则看不出来是否因ethtool引起的闪烁；  