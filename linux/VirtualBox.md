### vdi文件调整大小后进系统剩余空间还是原来的，使用如下命令就调整好了：
> sudo resize2fs /dev/sda6

# 使用VirtualBox把IMG文件转换为VDI文件
```
语法：
#转换为原大小的动态磁盘
VBoxManage convertdd input.img output.vdi
#转换为自定义大小的固定磁盘
VBoxManage convertdd source.img destination.vdi --variant Fixed
--variant Fixed 表示将生成固定分配的磁盘。
```
# 嵌套虚拟化
`VBoxManage modifyvm 虚拟机名字 --nested-hw-virt on`

# USB不能用
添加用户权限：

1、添加usbfs用户组（装完成后会有vboxusers和vboxsf 两个用户组）

`sudo groupadd usbfs`

2、将你的Linux常用用户添加到vboxusers、usbfs这个两个组中
    
      sudo adduser jack vboxusers  
      sudo adduser jack usbfs 
---
# 启动安装CentOS,CDliunx时报错：
产生原因：没有配置支持PAE/NX扩展特性  
解决方法：在VirtualBox界面上，选中虚拟机，打开设置 -> 系统 -> 处理器，在“扩展特性”选项处，勾选“启用PAE/NX”即可。  

---
针对Unable to load R3 module 解决方案,问题出现是因为破解了uxtheme.dll文件所以没办法打开，所以下载一个uxtheme.dll替换就好了，或者把系统自己备份的uxtheme.dll.backup 改回去就成了

---
# 与XP进行文件共享
  1. 首次安装的话，启动虚拟机，选菜单“设备”下的“安装增强功能”，下载iso，安装。
  2. 宿主添加共享文件夹，XP打开“我的电脑”，选菜单“工具”下的“映射网络驱动器”。添加

