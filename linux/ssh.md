### CentOS修改端口
```
#修改配置文件，添加2222
vi /etc/ssh/sshd_config

# If you want to change the port on a SELinux system, you have to tell
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#保留两个端口
Port 22
Port 2222

#防火墙放行
firewall-cmd --zone=public --add-port=2222/tcp --permanent
firewall-cmd --reload

#SELinux添加修改的SSH端口，如果不修改SELinux直接重启sshd，会出现Bind to port 2222 on 0.0.0.0 failed: Permission denied错误
#安装管理工具
yum -y install policycoreutils-python
#新增selinux中sshd的端口
semanage port -a -t ssh_port_t -p tcp 2222

#重启服务
systemctl restart sshd
```

### 修改端口
```
sudo nano /etc/ssh/sshd_config 
删掉注释，将22改为2222，保存并退出
/etc/init.d/ssh restart 重启ssh服务
```

### 防火墙上放开2222端口
```
sudo iptables -I INPUT -p tcp -m state --state NEW -m tcp --dport 2222 -j ACCEPT 
sudo iptables-save
```
### 查看2222端口是否已经正常开放
iptables -nL --line-number

### 重启sshd服务
 systemctl restart sshd.service

### 免密登录
```
ssh-keygen 三次回车，在.ssh下生成id_rsa和id_rsa.pub
ssh-copy-id -i ~/.ssh/id_rsa.pub -p 2222 用户名@服务器ip 把公钥发送给服务器
ssh -p 2222 用户名@服务器ip 以后就这样登录
```