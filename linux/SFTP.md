# sftp命令
sftp命令是一款交互式的文件传输程序，sftp命令对传输的所有信息使用ssh加密，它还支持公钥认证和压缩等功能。

## 连接
```
sftp -o Port=2222 username@remote ip(or remote host name)
sftp -o Port=2222 admin@www.xxxxxxx.com  或者 sftp admin@192.168.1.3 
然后命令提示符下输入sftp的密码。
```
## 上传文件
```
sftp -oPort=23 <username>@<remote> 
put /path/filename(本地主机) /path/filename(远端主机)
```
## 下载文件
```
sftp -oPort=23 <username>@<remote> 
get /path/filename(远端主机) /path/filename(本地主机)
```
## 本地和远端操作命令区别

在sftp的环境下的操作就和一般ftp的操作类似了，ls,rm,mkdir,dir,pwd,等指令都是对远端进行操作，如果要对本地操作，只需在上述的指令上加‘l’变为：lls,lcd, lpwd等。
