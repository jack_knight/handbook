### 安装 CPUS-PDF

> sudo apt install -y cups-pdf

打印看一下，在/home/用户/PDF目录下有文件就成功了，如果不行，操作下面的：
```
根据 CPUS-PDF的文档说明, 需要设置 cups-pdf 的所有者为 root,且权限为 0700

sudo chown root:root /usr/lib/cups/backend/cups-pdf
sudo chmod 0700 /usr/lib/cups/backend/cups-pdf

重启CUPS服务

sudo systemctl restart cups.service

可以通过修改 /etc/cups/cups-pdf.conf 的 OUT 参数来设置pdf的输出位置

### Key: Out (config)                                                        
##  CUPS-PDF output directory                                                
##  special qualifiers:                                                      
##     ${HOME} will be expanded to the user's home directory                 
##     ${USER} will be expanded to the user name                             
##  in case it is an NFS export make sure it is exported without             
##  root_squash!                                                             
##  on Ubuntu, the cupsd AppArmor profile needs to be updated                
##  to match the output path as per instructions in LP:147551                
### Default: /var/spool/cups-pdf/${USER}                                     

Out ${HOME}/PDF                                                              
```