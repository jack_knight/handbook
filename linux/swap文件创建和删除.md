# 创建
```
创建要作为swap分区的文件:增加1GB大小的交换分区，则命令写法如下，其中的count等于想要的块的数量（bs*count=文件大小）。 
sudo dd if=/dev/zero of=/swapfile bs=1M count=2048

格式化为交换分区文件: 
sudo mkswap /swapfile #建立swap的文件系统

启用交换分区文件: 
sudo swapon /swapfile #启用swap文件

检查swap是否已打开。 
cat /proc/swaps #或者free命令

使系统开机时自启用，在文件/etc/fstab中添加一行： 
/swapfile swap swap defaults 0 0
```
# 卸载删除
```
#卸载swap文件
sudo swapoff /swapfile 

#从/etc/fstab配置文件中删除
sudo nano /etc/fstab 

#删除文件
sudo rm -rf /swapfile
```