```
安装不了isomaster时，可以使用mkisofs。
在大多数现代 Linux 发行版中，这个工具通常包含在 genisoimage 包中。

#安装genisoimage
sudo apt-get install genisoimage

#打包为iso文件
mkisofs -o output.iso -V "ISO_Label" -J -r /path/to/directory
    -o output.iso: 指定输出的 ISO 文件名。
    -V "ISO_Label": 指定 ISO 文件的卷标（标签），最多 32 个字符。
    -J: 启用 Joliet 扩展，支持长文件名和 Unicode 字符。
    -r: 启用 Rock Ridge 扩展，保持 Unix 文件权限和符号链接。
    /path/to/directory，里面包含了你想要打包成 ISO 文件的所有文件。
```