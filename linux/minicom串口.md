sudo minicom -s 进入配置  
选serial port setup  修改  

输入A配置串口驱动为/dev/ttyUSB0  
输入E配置速率为9600 8N1  
输入F将 Hardware Flow Control 设 为 NO  

选Save setup as df1保存  

    sudo minicom

进入后，可以按Ctrl+A，放开Ctrl，再按Z，可以看到所有功能的提示  
---

非正常关闭minicom，会在/var/lock下创建几个文件LCK*，这几个文件阻止了minicom的运行，将它们删除后即可恢复  

有些电脑不能保存/dev/ttyS0，每次都要修改，保存，EXIT，然后可用。  

USB转串口线，linux自带驱动，插上即可用。  

