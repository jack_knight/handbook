# 安装

yum install iscsi-initiator-utils

# 侦测

iscsiadm -m discovery -t sendtargets -p 192.168.0.1

会提示无果，去存储里把发现的liunx加入，再侦测，有了。

# 登入

iscsiadm -m node -T iqn.2011-08.me.yn.cn:mydisk --login

# 分区

fdisk -l 

看到盘符是sdb，磁盘小于2T，用fdisk /dev/sdb

如果磁盘大于2T，要用parted /dev/sdb

# 格式化

mkfs.ext3 /dev/sdb1

如果磁盘有几T，要用快速格式化：mkfs.ext3 -T largefile /dev/sdb1，不然10T的盘要几天才格得完（血的教训*_*）。

# 挂载
```
mkdir /mnt/sdb1
mount /dev/sdb1 /mnt/sdb1
df
```
