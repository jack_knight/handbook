### 1.使用本地会话.

### 2.使用 nohup 执行.
* 在命令头尾分别加上 nohup 和 &，变为 nohup XXX.XXX &
* 通过 tail -f nohup.out 可以持续的查看 nohup.out 的输出，达到监视程序的效果。
* 在命令中也可以使用重定向将程序的输出改为自己想要的文件名，如 nohup bash hello.sh >hello.log &，则程序的输出就会写到 hello.log 文件中。
* 若程序不会自动退出，那么此时需要使用 kill 命令来结束进程。比如，可以使用命令 kill -TRM <PID> 来操作，其中 PID 即为之前 nohup 输出的值，在此例中该值为 1231。
* 使用限制：nohup 通常用于执行无干预的自动化程序或脚本，无法完成带有交互的操作。

### 3.使用 screen 执行
* 安装 sceen 工具,Linux 系统默认未自带 screen 工具，需要先进行安装：

    CentOS 系列系统： yum install screen

    Ubuntu 系列系统： sudo apt-get install screen

1. 创建 screen 窗口

> screen -S  name

name可以设置为ssh、ftp，用于标注该 screen 窗口用途

示例：screen -S ftp

2. 列出 screen 进程，并进入所需 screen

> screen -ls  ##列出 screen 进程列表

然后进行所需操作，比如运行脚本、执行程序等等。

3. 退出保存

前述 ftp 操作示例开始传输后，在窗口中键入Ctrl+a 键，再按下 d 键，就可以退出 SSH 登录，但不会影响 screen 程序的执行。

4. 保存会话以便继续执行

可以利用 screen 这种功能来管理的远程会话。操作步骤概述：

    正常 SSH 登录服务器

    创建 screen 窗口

    执行所需任务

    需要临时中断退出时，按 Ctrl +d 保存退出

    需要继续工作时，再次 SSH 登录服务器，然后直接执行 screen -r -d 恢复会话即可。
