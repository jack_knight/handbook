## 安装kali
## 更换源
```
sudo cp /etc/apt/sources.list /etc/apt/sources.list.bak
sudo nano /etc/apt/sources.list
可用mirrors.aliyun.com或http://mirrors.163.com/debian
sudo apt-get update
```
## 安装新立得
        sudo apt-get install -y synaptic

## 安装中文输入法
在新立得中搜索pinyin后安装fcitx-pinyin，注销后可用。如不可用，终端运行im-config，再注销。

## 浏览器中文
Firefox设置中第一项，language选China

## 开启root用户
```
 sudo su
<输入当前用户的密码>
passwd root
<输入root密码并再次输入该密码>
```
之后您可以注销,以root登录。
