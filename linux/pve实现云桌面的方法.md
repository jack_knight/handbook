# 创建虚拟机
虚拟机编号从100开始，可以规划为编号、ip、端口一致

# 使用客户端连接spice
1. 关闭虚拟机

2. 在web界面中把虚拟机的“硬件”-“显示”设置回原来的“默认”。

3. 编辑虚拟机的配置文件 
> nano /etc/pve/qemu-server/XXX.conf

加入
> args: -device AC97,addr=0x18 -spice 'port=6101,disable-ticketing,seamless-migration=on'

其中，-device选项是配置声卡，来自官方wiki。-spice选项是配置SPICE，这里绑定地址默认是0.0.0.0，所以不用设置。而port选项是设置SPICE的监听端口，这个端口应该是6000以上，5000都不行，后面在client中填写端口跟这里一致即可。disable-ticketing是关闭认证，这样子就可以不输入账号密码直接连上。seamless-migration这个选项不知道干什么用的。

可以直接这样写：
> echo "args: -spice 'port=6101,disable-ticketing,seamless-migration=on' >> /etc/pve/qemu-server/***.conf

4. virtviewer中输入
> spice://172.21.105.108:6101

或终端输入:
> remote-viewer -f spice://86.200.1.3:6102

5. 要想开机直接连上云桌面，可以在/home/.profile中加入：
> remote-viewer -f spice://86.200.1.3:6102

我在用lubuntu时，这样加云桌面会启动的太早，比窗口管理器还早，导致有问题。可以建一个脚本文件boot.sh，写入：
> sleep 5
remote-viewer-fspice://86.200.1.3:6102

然后在/home/.profile中加入：boot.sh &

---
windows下网页打开控制台，输密码时总提示大写锁定，密码错误，linux下正常。
