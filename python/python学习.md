### 输出不换行
> print('Hello',end='')

### 强制转换数据类型
int（2.0）
float（2）

### 运算符
//整除，**次方，%取余

### 分数
为启用 fractions 模块，必先引入  fractions  模块
```
import fractions
x = fractions.Fraction(1, 3)
```
---
程序中有中文时如报错，在首行加入注释：
> \# coding=utf-8


‘’‘三引号的字符串，可换行，可保留格式。

字符串前加r，为自然字符串，忽略转义字符'\'的功能。



