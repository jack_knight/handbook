### pyinstaller

在linux只能转linux可执行文件，exe要到windows下转，python 3.9不支持win7及以前的系统。  
```
pip3 install pyinstaller  安装

linux如果运行pyinstaller时出现“PyInstaller：未找到命令”的问题，可能是因为系统无法链接到pyinstaller的运行程序，解决办法如下。
1、找到pyinstaller的位置。在终端输入以下命令：

find / -name "pyinstaller"

之后会出现一大串的内容，在这一大串内容中找到pyinstaller的位置信息，形如/home/username/.local/bin/pyinstaller，其中username替换成自己的用户名。
2、把/home/username/.local/bin/pyinstaller复制到/usr/bin/pyinstaller，命令如下：

sudo cp /home/**username**/.local/bin/pyinstaller /usr/bin/pyinstaller

然后就可以运行pyinstaller了。
如果还不能运行，就加上pyinstaller的绝对路径再运行，即把运行pyinstaller的代码"pyinstaller -i xxx.ico xxx.py"改为"/home/**username**/.local/bin/pyinstaller -i xxx.ico xxx.py"
```
---

pyinstaller -F -w name.py  
-F 编译为一个文件
-w 执行时不显示cmd

使用upx可再减少一半体积。