```
#推荐安装Anaconda，一步到位，包括了要用到的几千个包，特别是内网windows。Win7系统不支持python3.9,也就是说win7可以安装python3.8.x最高版本的Anaconda,对应的是 Anaconda 2021.05。

#安装python3，一般linux默认安装了python。
#window安装时勾选path，安装python3.5以上,3.5以下不支持openpyxl，winxp只能安装3.4以下，所以winxp不能打包操作excel。
#linux可以不安装pip，直接用apt安装大部分包：
sudo apt-get install python3-pandas
sudo apt install python3-docx
sudo apt-get install python3-openpyxl

#IDE使用geany  
#在[生成][设置生成命令]将python改为python3

#安装pip3
#可在新立得中搜索python3-pip安装,或在终端安装:
sudo apt-get install python3-pip

#windows不用单独安装pip，如果输入pip提示“不是内部或外部命令”，在环境变量中path中py路径追加“\Scripts”

#pip3配置，pip慢，修改源。
#Linux下，首选，先升级 pip
pip3 install -i https://mirrors.ustc.edu.cn/pypi/web/simple pip -U
# 修改
pip3 config set global.index-url https://mirrors.ustc.edu.cn/pypi/web/simple

#次选：修改 ~/.pip/pip.conf (没有就创建一个文件夹及文件。文件夹要加“.”，表示是隐藏文件夹)，内容如下：

[global]
index-url = https://mirrors.ustc.edu.cn/pypi/web/simple
[install]
trusted-host = mirrors.ustc.edu.cn

#windows下在个人用户目录下c:\users\[自己的电脑用户名]\中新建文件夹 pip，再新建文件pip.ini,内容同上。

#office，python3操作docx不能安装docx，会报错。应该安装pip3 install python-docx
pip3 install openpyxl

#操作图片，使用PIL，需要安装Pillow
pip3 install Pillow
# 图形界面，新立得安装tkinter
```