## pandas插件import出错
```
#首选安装
sudo apt-get install python3-pandas

#不推荐安装，慢
pip3 install pandas
默认会安装最新版，可能会版本不兼容，导致导包出现错误。
 可以适当降低pandas版本。
 先在cmd输入此命令卸载：

pip3 uninstall pandas
 然后下面问题全选y
然后再安装(使用镜像，安装1.0.1版本)：


pip3 install pandas==1.0.1
如还不行再按照上面方法降低版本号试试（1.0.0）.
 
可能还会提示要安装pip3 install xlrd,xlrd可以处理超链接,但高版本不能打开xlsx文件
pip3 install xlrd==1.2.0
```