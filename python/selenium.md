### 安装selenium
pip3 install selenium
### 安装浏览器驱动
针对不同的浏览器，需要安装不同的驱动 

Firefox 浏览器驱动：https://github.com/mozilla/geckodriver/releases 

Chrome 浏览器驱动：https://npmmirror.com/mirrors/chromedriver/

Edge 浏览器驱动：https://developer.microsoft.com/en-us/microsoft-edge/tools/webdriver/ 

将驱动放在/usr/local/bin下，或将路径写在程序中