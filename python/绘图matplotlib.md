```
如果绘图出来汉字是方框，是因为找不到字体。
到/home/ethan/.cache/matplotlib目录下打开fontlist-v330.json
在其中搜索到汉字字体，例：
    {
      "fname": "/usr/share/fonts/wps-office/FZHTK.TTF",
      "name": "FZHei-B01",
      "style": "normal",
      "variant": "normal",
      "weight": 400,
      "stretch": "normal",
      "size": "scalable",
      "__class__": "FontEntry"
    },
将字体名称FZHei-B01在代码中替换：
plt.rcParams['font.sans-serif'] = ['FZHei-B01']
#字体名称最好不要有空格
```