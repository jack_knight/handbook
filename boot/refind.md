### 安装
```
sudo apt-add-repository ppa:rodsmith/refind
sudo apt-get update
sudo apt-get install refind
```

refind安装的目录在boot/efi/efi/refindRefind 

所有的配置信息位于/boot/efi/EFI/refind/refind.conf 

文件 refind.conf 中的#后面的部分都被注释掉的，想要使用那一行的配置可以把 # 去掉

### 常用的配置
```
timeout 20 等待时间 
hideui all 引导界面的那些小东西全部去掉 
resolution 1024 768 分辨率 
dont_scan_dirs efi/boot 如果有一些其他的启动项可以用这个隐藏掉 
scan_all_linux_kernels false 如果linux有多个内核版，可以用这个隐藏掉 
max_tags 0 启动项显示的个数 
default_selection "+,Microsoft,vmlinuz" 默认启动
+号表示选择上一次打开的选项,Microsoft表示windows系统，vmlinuz表示linux系统，
```