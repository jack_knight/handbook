1. 打开“开始→运行”，输入“regedit”，打开注册表，
进入以下路径：
> [HKEY_LOCAL_MACHINE\SYSTEM\ CurrentControlSet\Control\Terminal Server\ Wds\rdpwd\Tds\tcp]

看见PortNamber值了吗？其默认值是3389，修改成所希望的端口即可，例如3333。

2. 再打开
> [HKEY_LOCAL_MACHINE\SYSTEM\CurrentContro1Set\Control\Tenninal Server\WinStations\RDP-Tcp]

将PortNumber的值（默认是3389）修改成端口3333。

 3. 修改完毕，重新启动电脑，以后远程登录的时候使用**IP:3333**就可以了
