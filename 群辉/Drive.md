## Deepin 20系统，群晖Synology Drive客户端不能正常的启动运行

安装好Synology Drive客户端后，在终端里查看程序的运行位置：

which synology-drive

输出如下：

/usr/bin/synology-drive

看看这个文件里有什么：

sudo nano /usr/bin/synology-drive

如上图所示，文件开头就声明了安装路径，如果是Deepin操作系统，那么路径是/opt/apps/com.synology.drive/files/Synology/SynologyDrive。

但是实际上，synology Drive客户端是被安装在/opt/Synology/SynologyDrive的，可以自己进入这两个目录验证。

 

### 解决方法

可以直接去掉条件判断，指定INSTALL_PATH="/opt/Synology/SynologyDrive"就行。