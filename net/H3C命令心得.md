# 交换机清除配置 
```
<H3C>reset save 
<H3C>reboot
```
# 交换机忘记密码
```
重启
Ctrl+B 进bootrom
***
1. Download application file to flash ¬下载应用程序到Flash中
2. Select application file to boot ¬选择启动文件
3. Display all files in flash ¬显示Flash中所有文件
4. Delete file from flash ¬删除Flash中的文件
5. Modify bootrom password ¬修改BootROM密码
6. Enter bootrom upgrade menu ¬进入BootROM升级菜单
7. Skip current configuration file ¬设置重启不运行当前配置文件
8. Set bootrom password recovery ¬恢复BootROM密码
9. Set switch startup mode ¬设置交换机启动模式
0. Reboot ¬重新启动交换机
```