# 建vlan
[HUAWEI]vlan 1000 1001 2 3 4 5 6...

# 建子网
```
[HUAWEI] vlan 100
[HUAWEI-vlan100] ip-subnet-vlan 1 ip 192.168.1.2 24
[HUAWEI-vlan100] quit
```
# 建端口组
```
port－group ip
port-group group-member g0/0/1 to g0/0/24
```
# 设端口
```
[HUAWEI] interface gigabitethernet 0/0/6
[HUAWEI-GigabitEthernet0/0/6] ip-subnet-vlan enable
[HUAWEI-GigabitEthernet0/0/6] port link-type hybrid
[HUAWEI-GigabitEthernet0/0/6] port hybrid untagged vlan 1000 1001 2 3 4 5
[HUAWEI-GigabitEthernet0/0/6] port hybrid tagged vlan 2600
[HUAWEI-GigabitEthernet0/0/6] quit
```
